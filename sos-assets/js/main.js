
$(function() {
    $('.intro-about__content h1').click(function(){
        $(this).toggleClass('open');
        $('.intro-about__texts').slideToggle('fast'); 
    });

    $('.intro-open-hours__title').click(function(){
        $(this).toggleClass('open');
        $(this).parent().children('.intro-open-hours__content').slideToggle('fast'); 
    });
});